package com.brettmcgin.operationjerb.photo

import java.net.URL

data class Photo (
    val id: String,
    val title: String,
    val url: URL,
    val isPublic: Boolean
)
