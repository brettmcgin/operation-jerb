package com.brettmcgin.operationjerb.photo

interface PhotosRepository {
    suspend fun getPhotos(page: Int = 1): List<Photo>
}
