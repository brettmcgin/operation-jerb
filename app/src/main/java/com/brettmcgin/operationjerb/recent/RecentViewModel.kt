package com.brettmcgin.operationjerb.recent

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.brettmcgin.operationjerb.flickr.FlickrPhotosRepository
import com.brettmcgin.operationjerb.photo.Photo
import kotlinx.coroutines.launch

class RecentViewModel : ViewModel() {
    private val photoRepository = FlickrPhotosRepository()
    private val mutablePhotosLiveData = MutableLiveData<List<Photo>>()
    val photosLiveData: LiveData<List<Photo>> = mutablePhotosLiveData

    private var currentPage = 1
    private val photos: ArrayList<Photo> = arrayListOf()

    init {
        viewModelScope.launch { fetchPhotos() }
    }

    suspend fun fetchPhotos() {
        photos.addAll(photoRepository.getPhotos(currentPage))
        mutablePhotosLiveData.postValue(photos)
    }

    fun fetchNextPage() {
        currentPage++
        viewModelScope.launch { fetchPhotos() }
    }
}
