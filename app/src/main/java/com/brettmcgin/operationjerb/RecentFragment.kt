package com.brettmcgin.operationjerb

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.brettmcgin.operationjerb.recent.PhotosAdapter
import com.brettmcgin.operationjerb.recent.RecentViewModel
import kotlinx.android.synthetic.main.fragment_recent.*

class RecentFragment : Fragment() {
    private val photosViewModel: RecentViewModel by viewModels()
    private val photosAdapter = PhotosAdapter()
    private val args: RecentFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_recent, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = GridLayoutManager(this.context, args.columns)

        photosRecyclerView.layoutManager = layoutManager
        photosRecyclerView.adapter = photosAdapter

        photosRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val currentLast = layoutManager.findLastVisibleItemPosition()

                // Scrolling to 80% and then fetching is a little silly, but works for demo purposes
                if (currentLast >= (layoutManager.itemCount * .8)) {
                    photosViewModel.fetchNextPage()
                }
            }
        })

        photosViewModel.photosLiveData.observe(viewLifecycleOwner,
            Observer { list ->
                with(photosAdapter) {
                    photos.clear()
                    photos.addAll(list.filter { it.isPublic })
                    notifyDataSetChanged()
                }
            })
    }
}
