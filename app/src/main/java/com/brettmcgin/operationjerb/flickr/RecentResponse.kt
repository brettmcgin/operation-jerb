package com.brettmcgin.operationjerb.flickr

import com.google.gson.annotations.SerializedName

data class RecentResponse (
    @SerializedName("photos") val metaData: RecentMetaData
)
