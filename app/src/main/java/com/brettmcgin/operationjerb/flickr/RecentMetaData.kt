package com.brettmcgin.operationjerb.flickr

import com.google.gson.annotations.SerializedName

data class RecentMetaData (
    val page: Number,
    val pages: Number,
    @SerializedName("perpage") val perPage: Number,
    val total: Number,
    @SerializedName("photo") val photos: List<Photo>
)
