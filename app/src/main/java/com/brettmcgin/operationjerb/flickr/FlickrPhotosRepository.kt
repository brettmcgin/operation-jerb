package com.brettmcgin.operationjerb.flickr

import com.brettmcgin.operationjerb.photo.Photo
import com.brettmcgin.operationjerb.photo.PhotosRepository
import java.net.URL

class FlickrPhotosRepository : PhotosRepository {
    override suspend fun getPhotos(page: Int): List<Photo> {
        val recentResponse = FlickrClient.fetchRecent(page)
        return recentResponse.metaData.photos.map { photo ->
            Photo(
                id = photo.id,
                url = URL("https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg"),
                title = photo.title,
                isPublic = photo.isPublic == 1
            )
        }
    }
}
