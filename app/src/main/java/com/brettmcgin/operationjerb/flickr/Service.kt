package com.brettmcgin.operationjerb.flickr

import com.brettmcgin.operationjerb.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


private const val BASE_URL = "https://api.flickr.com/"
private const val API_KEY = BuildConfig.FLICKR_API_KEY
private const val CONNECTION_TIMEOUT_MS: Long = 10

interface FlickrService {
    @GET("services/rest")
    @Headers("Content-Type: application/json")
    suspend fun fetchRecent(
        @Query("api_key") apiKey: String = API_KEY,
        @Query("format") format: String = "json",
        @Query("method") method: String = "flickr.photos.getRecent",
        @Query("nojsoncallback") callback: String = "?",
        @Query("page") page: Int
    ): RecentResponse
}

object FlickrClient {
    private val client: FlickrService by lazy {
        Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(
                OkHttpClient.Builder()
                    .connectTimeout(CONNECTION_TIMEOUT_MS, TimeUnit.SECONDS)
                    .build()
            )
            .build()
            .create(FlickrService::class.java)
    }

    suspend fun fetchRecent(page: Int) = client.fetchRecent(page = page)
}
